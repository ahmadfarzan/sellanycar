<?php

$input = explode(" ", file_get_contents('input.txt'));

$sorted = array();

$sorted['merge'] = $input;
usort($sorted['merge'], array('SortIt', 'merge'));

$sorted['insertion'] = SortIt::insertion($input);

$sorted['selection'] = SortIt::selection($input);


echo "<table>";
echo "<tr><th>Unsorted</th><td>" . implode(" ", $input) . "</td></tr>";
foreach ($sorted as $k => $s) {
    echo "<tr><th>" . ucfirst($k) . "</th><td>" . implode(" ", $s) . "</td></tr>";
}
echo "</table>";

class SortIt {

    public static function merge($elemA, $elemB) {
        return ($elemA == $elemB) ? 0 : ($elemA < $elemB) ? -1 : 1;
    }

    public static function insertion($array) {
        for ($i = 1; $i < count($array); $i++) {
            $current_element = $array[$i];
            $j = $i;
            while ($j > 0 and $array[$j - 1] > $current_element) {
                $array[$j] = $array[$j - 1];
                $j = $j - 1;
            }
            $array[$j] = $current_element;
        }
        return $array;
    }

    public static function selection($array) {
        for ($i = 0; $i < count($array); $i++) {
            $min = false;
            $minKey = false;
            for ($j = $i; $j < count($array); $j++) {
                if ($array[$j] < $min or $min === false) {
                    $minKey = $j;
                    $min = $array[$minKey];
                }
            }
            $array[$minKey] = $array[$i];
            $array[$i] = $min;
        }
        return $array;
    }

}
